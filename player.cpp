#include "player.h"
#include <climits>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    this->side = side;
    other = (side == BLACK) ? WHITE : BLACK;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}


int Player::score(Move *m, Side mover, Board *b)
{    
    int x = m->x, y = m->y;
    int score = b->count(side) - b->count(other);

    if (testingMinimax)
    {
        return score;
    }
    
    if (mover == other)
    {
        return score;
    }
    if ((x == 0 && y == 0) ||
        (x == 0 && y == 7) ||
        (x == 7 && y == 0) ||
        (x == 7 && y == 7))
    {
        score *= 3;
    }
    else if ((x == 0 && y == 1) || (x == 1 && y == 0) ||
             (x == 0 && y == 6) || (x == 1 && y == 7) ||
             (x == 7 && y == 1) || (x == 6 && y == 0) ||
             (x == 7 && y == 6) || (x == 6 && y == 7))
    {
        score *= -3;
    }
    else if (x == 0 || x == 7 || y == 0 || y == 7)
    {
        score *= 2;
    }
    return score;
}


Result Player::minimax(Board *b, int depth, bool self, Move *last, int alpha, int beta)
{
    if (depth == 0)
    {
        return Result(NULL, score(last, self ? other : side, b));
    }
    if (self)
    {
        int maxScore = INT_MIN;
        Move maxMove(-1, -1);
        bool hasMove = false;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                Move move(x, y);
                if (b->checkMove(&move, side))
                {
                    hasMove = true;
                    Board child(*b);
                    child.doMove(&move, side);
                    Result res =
                        minimax(&child, depth-1, false, &move, alpha, beta);
                    if (res.move != NULL)
                    {
                        delete res.move;
                    }
                    if (res.score > maxScore)
                    {
                        maxScore = res.score;
                        maxMove = Move(move);
                    }
                    alpha = (maxScore > alpha) ? maxScore : alpha;
                    if (beta < alpha)
                    {
                        break;
                    }
                }
            }
            if (beta < alpha)
            {
                break;
            }
        }
        if (hasMove)
        {
            return Result(new Move(maxMove), maxScore);
        }
        else
        {
            return Result(NULL, score(last, self ? other : side, b));
        }
        
    }
    else
    {
        int minScore = INT_MAX;
        Move minMove(-1, -1);
        bool hasMove = false;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                Move move(x, y);
                if (b->checkMove(&move, other))
                {
                    hasMove = true;
                    Board child(*b);
                    child.doMove(&move, other);
                    Result res =
                        minimax(&child, depth-1, true, &move, alpha, beta);
                    if (res.move != NULL)
                    {
                        delete res.move;
                    }
                    if (res.score < minScore)
                    {
                        minScore = res.score;
                        minMove = Move(move);
                    }
                    beta = (minScore < beta) ? minScore : beta;
                    if (beta < alpha)
                    {
                        break;
                    }
                }
            }
            if (beta < alpha)
            {
                break;
            }
        }
        if (hasMove)
        {
            return Result(new Move(minMove), minScore);
        }
        else
        {
            return Result(NULL, score(last, self ? other : side, b));
        }
    }
}


/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    board.doMove(opponentsMove, other);
    int alpha = INT_MIN;
    int beta = INT_MAX;
    Result res = minimax(&board, 8, true, opponentsMove, alpha, beta);
    /*
    if (res.move == NULL)
    {
        cerr << "no moves!" << endl;
    }
    else
    {
        cerr << "picked move: " << res.move->x << ", " << res.move->y << endl;
    }
    */
    board.doMove(res.move, side);
    return res.move;
}

void Player::setBoard(char data[])
{
    board.setBoard(data);
}
