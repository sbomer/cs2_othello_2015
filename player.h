#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

struct Result {
    Move *move;
    int score;
    Result(Move *move, int score)
    {
        this->move = move;
        this->score = score;
    };
};

class Player {
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    void setBoard(char data[]);
private:
    Side side;
    Board board;
    Side other;
    int score(Move *m, Side mover, Board *b);
    Result minimax(Board *b, int depth, bool self, Move *last, int alpha, int beta);
};


#endif
