I'm working solo.

For the first week, I wrote a minimax algorithm that was hard-coded to
a 2-ply depth. This week I improved this by making it take a depth
parameter, so it now calls itself recursively. I also implemented
alpha-beta pruning, and used compiler optimizations to speed up my
program.
